package com.example.shops.model

import java.io.Serializable

data class Address(
    val address: String = "",
    val phone: String = "",
    val schedule: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
) : Serializable