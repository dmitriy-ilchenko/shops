package com.example.shops.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Shop(
    val id: Int = Int.MIN_VALUE,
    val name: String = "",
    @SerializedName("short_description") val shortDescription: String = "",
    val description: String = "",
    val image: String = "",
    val rating: Double = 0.0,
    val addresses: List<Address> = emptyList()
) : Serializable