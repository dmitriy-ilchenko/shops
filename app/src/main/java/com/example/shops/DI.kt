package com.example.shops

import com.example.shops.repository.MockShopRepository
import com.example.shops.repository.NetworkShopRepository
import com.example.shops.repository.ShopRepository

object RepositoryModule {
    val shopRepository: ShopRepository = NetworkShopRepository()
}