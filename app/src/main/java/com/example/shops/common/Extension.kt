package com.example.shops.common

import android.view.View
import java.util.*

fun Double?.toOneDecimalPlace(): String {
    return if (this == null) {
        "0.0"
    } else {
        String.format(Locale.ENGLISH,"%.1f", this)
    }
}

fun Boolean.toVisibility(): Int {
    return if (this) View.VISIBLE else View.GONE
}