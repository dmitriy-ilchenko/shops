package com.example.shops.common

import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.util.Collections.emptyList

object PermissionsUtil {

    private fun filterNotGrantedPermissions(context: Context, permissions: List<String>): List<String> {
        return permissions.filter { permission ->
            ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED
        }
    }

    fun requestPermissions(activity: AppCompatActivity, permissions: Array<String>, permissionsRequestCode: Int) {
        val notGrantedPermissions = filterNotGrantedPermissions(activity, permissions.toList())
        if (notGrantedPermissions.isNotEmpty()) {
            ActivityCompat.requestPermissions(activity, permissions, permissionsRequestCode)
        }
    }

    fun handlePermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: Array<Int>, permissionsRequestCode: Int): Pair<List<String>, List<String>> {
        if (requestCode != permissionsRequestCode) {
            return Pair(emptyList(), emptyList())
        }

        val grantedPermissions = mutableListOf<String>()
        val notGrantedPermissions = mutableListOf<String>()

        for (i in grantResults.indices) {
            val grantResult = grantResults[i]
            val permission = permissions[i]

            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                grantedPermissions.add(permission)
            } else {
                notGrantedPermissions.add(permission)
            }
        }

        return Pair(grantedPermissions, notGrantedPermissions)
    }
}