package com.example.shops.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shops.R
import com.example.shops.RepositoryModule
import com.example.shops.adapter.ShopListAdapter
import com.example.shops.common.EmptyTextWatcher
import com.example.shops.common.toVisibility
import com.example.shops.model.Shop
import com.example.shops.viewmodel.ShopSearchViewModel
import com.example.shops.viewmodel.ShopSearchViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_shop_search.*

class ShopSearchActivity : BaseChildActivity() {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ShopSearchActivity::class.java)
            context.startActivity(intent)
        }
    }


    private val shopListAdapter = ShopListAdapter(::startShopActivity)

    private lateinit var viewModel: ShopSearchViewModel
    private val viewModelDisposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop_search)
        createViewModel()
        initActionBar()
        initShopList()
        initEventHandlers()
    }

    override fun onStart() {
        super.onStart()
        subscribeToViewModel()
    }

    override fun onStop() {
        super.onStop()
        unsubscribeFromViewModel()
    }


    private fun initActionBar() {
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDefaultDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            setHomeButtonEnabled(true)
        }
    }

    private fun initShopList() {
        recyclerViewShopList.layoutManager = LinearLayoutManager(this)
        recyclerViewShopList.adapter = shopListAdapter
    }

    private fun initEventHandlers() {
        editTextSearch.addTextChangedListener(object : EmptyTextWatcher() {
            override fun onTextChanged(searchText: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.handleSearchTextChanged(searchText.toString())
            }
        })

        buttonClearSearchText.setOnClickListener { editTextSearch.setText("") }
    }


    private fun createViewModel() {
        viewModel = ViewModelProviders
            .of(this, ShopSearchViewModelFactory(RepositoryModule.shopRepository))
            .get(ShopSearchViewModel::class.java)
    }

    private fun subscribeToViewModel() {
        viewModel.isLoading
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::showIsLoading)
            .addTo(viewModelDisposable)

        viewModel.isError
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::showIsError)
            .addTo(viewModelDisposable)

        viewModel.isMainContentVisible
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::showIsMainContentVisible)
            .addTo(viewModelDisposable)

        viewModel.shopsSearchResult
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::showShopsSearchResult)
            .addTo(viewModelDisposable)
    }

    private fun unsubscribeFromViewModel() {
        viewModelDisposable.clear()
    }


    private fun showIsLoading(isLoading: Boolean) {
        progressBarLoading.visibility = isLoading.toVisibility()
    }

    private fun showIsError(isError: Boolean) {
        layoutError.visibility = isError.toVisibility()
    }

    private fun showIsMainContentVisible(isVisible: Boolean) {
        recyclerViewShopList.visibility = isVisible.toVisibility()
    }

    private fun showShopsSearchResult(searchResult: Pair<String, List<Shop>>) {
        val searchText = searchResult.first
        val shops = searchResult.second
        val textInSearchEditText = editTextSearch.text.toString()

        if (textInSearchEditText != searchText) {
            return
        }

        shopListAdapter.setSearchText(searchText)
        shopListAdapter.setData(shops)
    }


    private fun startShopActivity(shopId: Int) {
        ShopDetailsActivity.start(this, shopId)
    }
}