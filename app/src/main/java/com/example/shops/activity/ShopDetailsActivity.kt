package com.example.shops.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.shops.R
import com.example.shops.RepositoryModule
import com.example.shops.common.toOneDecimalPlace
import com.example.shops.common.toVisibility
import com.example.shops.model.Address
import com.example.shops.model.Shop
import com.example.shops.viewmodel.ShopDetailsViewModel
import com.example.shops.viewmodel.ShopDetailsViewModelFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_shop_details.*

class ShopDetailsActivity : BaseChildActivity() {

    companion object {
        private const val shopIdIntentKey = "shopIdIntentKey"

        fun start(context: Context, shopId: Int) {
            val intent = Intent(context, ShopDetailsActivity::class.java)
            intent.putExtra(shopIdIntentKey, shopId)
            context.startActivity(intent)
        }
    }

    private lateinit var viewModel: ShopDetailsViewModel
    private val viewModelDisposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop_details)
        initEventHandlers()
        createViewModel()
    }

    override fun onStart() {
        super.onStart()
        subscribeToViewModel()
    }

    override fun onStop() {
        super.onStop()
        unsubscribeFromViewModel()
    }


    private fun initEventHandlers() {
        buttonLoadShop.setOnClickListener { viewModel.loadShop() }
    }


    private fun createViewModel() {
        val shopId = intent.getIntExtra(shopIdIntentKey, 0)
        viewModel = ViewModelProviders
            .of(this, ShopDetailsViewModelFactory(RepositoryModule.shopRepository, shopId))
            .get(ShopDetailsViewModel::class.java)
    }

    private fun subscribeToViewModel() {
        viewModel.isLoading.subscribe(::showIsLoading).addTo(viewModelDisposable)
        viewModel.isError.subscribe(::showIsError).addTo(viewModelDisposable)
        viewModel.isMainContentVisible.subscribe(::showIsMainContentVisible).addTo(viewModelDisposable)
        viewModel.shop.subscribe(::showShop).addTo(viewModelDisposable)
    }

    private fun unsubscribeFromViewModel() {
        viewModelDisposable.clear()
    }


    private fun showIsLoading(isLoading: Boolean) {
        progressBarLoading.visibility = isLoading.toVisibility()
    }

    private fun showIsError(isError: Boolean) {
        layoutError.visibility = isError.toVisibility()
    }

    private fun showIsMainContentVisible(isVisible: Boolean) {
        layoutMainContent.visibility = isVisible.toVisibility()
    }

    private fun showShop(shop: Shop) {
        shopPhoto(shop.image)
        showBasicInfo(shop)
        showDescription(shop.description)
        showAddresses(shop.addresses)

        buttonShowAddressesOnMap.setOnClickListener { ShopAddressesOnMapActivity.start(this, shop) }
    }

    private fun shopPhoto(imageUrl: String) {
        Glide
            .with(this)
            .load(imageUrl)
            .apply(RequestOptions().placeholder(R.color.silver))
            .into(imageViewPhoto)
    }

    private fun showBasicInfo(shop: Shop) {
        supportActionBar?.title = shop.name
        textViewName.text = shop.name
        textViewShortDescription.text = shop.shortDescription
        textViewRating.text = shop.rating.toOneDecimalPlace()
    }

    private fun showDescription(description: String) {
        if (description.isEmpty()) {
            return
        }

        textViewDescriptionHint.visibility = View.VISIBLE
        textViewDescription.visibility = View.VISIBLE
        textViewDescription.text = description
    }

    private fun showAddresses(addresses: List<Address>) {
        if (addresses.isEmpty()) {
            return
        }

        textViewAddressesHint.visibility = View.VISIBLE
        textViewAddresses.visibility = View.VISIBLE
        buttonShowAddressesOnMap.visibility = View.VISIBLE

        val addressesText = addresses.joinToString("\n\n") {
            "${getString(R.string.address)} ${it.address}\n" +
            "${getString(R.string.phone)} ${it.phone}\n" +
            "${getString(R.string.schedule)} ${it.schedule}"
        }
        textViewAddresses.text = addressesText
    }
}