package com.example.shops.activity

import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity

abstract class BaseChildActivity : AppCompatActivity() {
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}