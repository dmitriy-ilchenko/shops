package com.example.shops.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.shops.R
import com.example.shops.common.PermissionsUtil
import com.example.shops.fragment.dialog.AddressBottomSheetDialogFragment
import com.example.shops.model.Address
import com.example.shops.model.Shop
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions


class ShopAddressesOnMapActivity : BaseChildActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    companion object {
        val shopIntentKey = "shopIntentKey"

        fun start(context: Context, shop: Shop) {
            val intent = Intent(context, ShopAddressesOnMapActivity::class.java)
            intent.putExtra(shopIntentKey, shop)
            context.startActivity(intent)
        }
    }


    private val permissionRequestCode = 101
    private val permissions = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)

    private lateinit var shop: Shop


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shop_addresses_on_map)
        requestPermissions()
        parseIntent()
        showShopName()
        getMapAsync()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            initMap(googleMap)
            googleMap.setOnMarkerClickListener(this)
            showAddressesOnMap(googleMap)
        }
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        val address = marker?.tag as Address?
        if (address != null) {
            showAddressBottomSheetDialog(address)
        }
        return true
    }


    private fun requestPermissions() {
        PermissionsUtil.requestPermissions(this, permissions, permissionRequestCode)
    }

    private fun parseIntent() {
        shop = intent.getSerializableExtra(shopIntentKey) as Shop
    }


    private fun getMapAsync() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun initMap(googleMap: GoogleMap) {
        googleMap.apply {
            mapType = GoogleMap.MAP_TYPE_NORMAL
            isTrafficEnabled = false
            isIndoorEnabled = false
            isBuildingsEnabled = true
            uiSettings.apply {
                isZoomControlsEnabled = true
                isZoomGesturesEnabled = true
            }
            setMyLocationButtonEnabled(this)
        }
    }

    private fun setMyLocationButtonEnabled(googleMap: GoogleMap) {
        try {
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = true
        } catch (ex: SecurityException) {
            ex.printStackTrace()
        }
    }


    private fun showShopName() {
        supportActionBar?.title = shop.name
    }

    private fun showAddressesOnMap(googleMap: GoogleMap) {
        val bounds = LatLngBounds.Builder()
        shop.addresses.forEach { address ->
            showAddressOnMap(googleMap, address)
            bounds.include(LatLng(address.latitude, address.longitude))
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 200))
    }

    private fun showAddressOnMap(googleMap: GoogleMap, address: Address) {
        val location = LatLng(address.latitude, address.longitude)
        val markerOptions = MarkerOptions()
            .position(location)
            .title(address.address)
        googleMap.addMarker(markerOptions).tag = address
    }

    private fun showAddressBottomSheetDialog(address: Address) {
        AddressBottomSheetDialogFragment.newInstance(address).show(supportFragmentManager, "")
    }
}