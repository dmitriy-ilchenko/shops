package com.example.shops.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.shops.R
import com.example.shops.adapter.TabPagerAdapter
import com.example.shops.fragment.ShopListFragment
import com.example.shops.fragment.ShopsMapFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initTabs()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.item_search) {
            navigateToShopSearchActivity()
        }
        return true
    }

    private fun initTabs() {
        val fragments = listOf(ShopListFragment(), ShopsMapFragment())
        val tabTitles = resources.getStringArray(R.array.tab_titles)
        val tabPagerAdapter = TabPagerAdapter(supportFragmentManager, tabTitles.asList(), fragments)

        viewPager.adapter = tabPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun navigateToShopSearchActivity() {
        ShopSearchActivity.start(this)
    }
}
