package com.example.shops.repository

import com.example.shops.model.Address
import com.example.shops.model.Shop
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MockShopRepository : ShopRepository {
    private val shops = createMockShops(10)


    override fun getAllShops(): Single<List<Shop>> {
        //return Single.error(Throwable())
        return Single
            .just(shops)
            .delay(2500, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
    }

    override fun getShopById(id: Int): Single<Shop> {
        //return Single.error(Throwable())
        return Single.create<Shop> { emitter ->
                val shop = shops.find { shop -> shop.id == id }
                if (shop != null) {
                    emitter.onSuccess(shop)
                } else {
                    emitter.onError(IllegalArgumentException())
                }
            }
            .delay(2500, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
    }

    override fun searchShops(searchText: String): Single<List<Shop>> {
        if (searchText.isEmpty()) {
            return Single.just(emptyList())
        }
        return getAllShops()
    }


    private fun createMockShops(count: Int): List<Shop> {
        val shops = mutableListOf<Shop>()
        for (id in 0..count) {
            shops.add(createMockShop(id))
        }
        return shops
    }

    private fun createMockShop(id: Int): Shop {
        return Shop(
            id,
            "Office $id",
            "Short description $id",
            "Description $id",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/1024px-Cat03.jpg",
            3.0 + id.toDouble() / 10,
            createMockAddresses(id)
        )
    }

    private fun createMockAddresses(id: Int): List<Address> {
        return listOf(
            Address("Address 1 $id", "88005553535", "8:00 - 17:00", 55.752121, 37.617664),
            Address("Address 2 $id", "89273946508", "9:00 - 18:00", 56.752121, 38.617664)
        )
    }
}