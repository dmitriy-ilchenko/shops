package com.example.shops.repository

import com.example.shops.BuildConfig
import com.example.shops.model.Shop
import com.example.shops.network.createRetrofitApi
import io.reactivex.Single

class NetworkShopRepository : ShopRepository {
    private val networkApi = createRetrofitApi(BuildConfig.baseUrl)
    private var shops: List<Shop> = emptyList()

    override fun getAllShops(): Single<List<Shop>> {
        return networkApi.getShops().doOnSuccess { shops = it }
    }

    override fun getShopById(id: Int): Single<Shop> {
        return Single.create { emitter ->
            val shop = shops.find { shop -> shop.id == id }
            if (shop != null) {
                emitter.onSuccess(shop)
            } else {
                emitter.onError(Throwable("Shop not found"))
            }
        }
    }

    override fun searchShops(searchText: String): Single<List<Shop>> {
        if (searchText.isEmpty()) {
            return Single.just(emptyList())
        }

        return Single.create { emitter ->
            val foundShops = shops.filter { shop ->
                shop.name.contains(searchText, true) || shop.shortDescription.contains(searchText, true)
            }
            emitter.onSuccess(foundShops)
        }
    }
}