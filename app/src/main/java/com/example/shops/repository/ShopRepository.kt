package com.example.shops.repository

import com.example.shops.model.Shop
import io.reactivex.Single

interface ShopRepository {
    fun getAllShops(): Single<List<Shop>>
    fun getShopById(id: Int): Single<Shop>
    fun searchShops(searchText: String): Single<List<Shop>>
}