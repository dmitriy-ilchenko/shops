package com.example.shops.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class TabPagerAdapter(
    fragmentManager: FragmentManager,
    private val tabTitles: List<String>,
    private val fragments: List<Fragment>
) : FragmentPagerAdapter(fragmentManager) {

    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return Math.min(tabTitles.count(), fragments.count())
    }
}