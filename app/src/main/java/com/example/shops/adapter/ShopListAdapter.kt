package com.example.shops.adapter

import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.style.BackgroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.shops.R
import com.example.shops.common.toOneDecimalPlace
import com.example.shops.model.Shop
import kotlinx.android.synthetic.main.item_shop.view.*

class ShopListAdapter(private val shopClickListener: (Int) -> Unit) : RecyclerView.Adapter<ShopListAdapter.ViewHolder>() {
    private var searchText = ""
    private var shops: List<Shop> = emptyList()


    fun setSearchText(searchText: String) {
        this.searchText = searchText
    }

    fun setData(shops: List<Shop>) {
        this.shops = shops
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.item_shop, parent, false)
        return ViewHolder(itemView, shopClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val shop = shops[position]
        holder.bind(shop, searchText)
    }

    override fun getItemCount(): Int {
        return shops.count()
    }


    class ViewHolder(itemView: View, private val shopClickListener: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {

        fun bind(shop: Shop, searchText: String) {
            itemView.setOnClickListener { shopClickListener(shop.id) }

            if (searchText.isEmpty()) {
                itemView.textViewName.text = shop.name
                itemView.textViewShortDescription.text = shop.shortDescription
            } else {
                itemView.textViewName.text = highlightText(shop.name, searchText)
                itemView.textViewShortDescription.text = highlightText(shop.shortDescription, searchText)
            }

            itemView.textViewRating.text = shop.rating.toOneDecimalPlace()

            Glide
                .with(itemView)
                .load(shop.image)
                .apply(RequestOptions().placeholder(R.color.silver))
                .into(itemView.imageViewPhoto)
        }

        private fun highlightText(text: String, searchText: String): SpannableString {
            val result = SpannableString(text)

            // Remove previous spans
            val backgroundSpans = result.getSpans(0, result.length, BackgroundColorSpan::class.java)
            for (span in backgroundSpans) {
                result.removeSpan(span)
            }

            // Highlight all searchText occurrences
            var indexOfKeyword = result.toString().indexOf(searchText, 0, true)
            while (indexOfKeyword >= 0) {
                //Create a background color span on the keyword
                result.setSpan(BackgroundColorSpan(Color.YELLOW), indexOfKeyword, indexOfKeyword + searchText.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

                //Get the next index of the keyword
                indexOfKeyword = text.indexOf(searchText, indexOfKeyword + searchText.length, true)
            }

            return result
        }
    }
}