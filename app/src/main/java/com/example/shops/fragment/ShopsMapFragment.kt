package com.example.shops.fragment

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.shops.R
import com.example.shops.RepositoryModule
import com.example.shops.common.PermissionsUtil
import com.example.shops.common.toVisibility
import com.example.shops.fragment.dialog.AddressBottomSheetDialogFragment
import com.example.shops.model.Address
import com.example.shops.model.Shop
import com.example.shops.viewmodel.ShopListViewModel
import com.example.shops.viewmodel.ShopListViewModelFactory
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_shops_map.*

class ShopsMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private val permissionRequestCode = 101
    private val permissions = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)

    private var mapFragment: SupportMapFragment? = null
    private lateinit var googleMap: GoogleMap

    private lateinit var viewModel: ShopListViewModel
    private val viewModelDisposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_shops_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestPermissions()
        initEventHandlers()
        getMapAsync()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unsubscribeFromViewModel()
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap ?: return
        initMap()
        subscribeToViewModel()
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        val (shopName, address) = marker?.tag as? Pair<String, Address> ?: return false
        showAddressBottomSheetDialog(shopName, address)
        return true
    }


    private fun requestPermissions() {
        PermissionsUtil.requestPermissions(requireActivity() as AppCompatActivity, permissions, permissionRequestCode)
    }

    private fun initEventHandlers() {
        buttonLoadShops.setOnClickListener { viewModel.loadShops() }
    }

    private fun getMapAsync() {
        mapFragment = childFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun initMap() {
        googleMap.apply {
            mapType = GoogleMap.MAP_TYPE_NORMAL
            isTrafficEnabled = false
            isIndoorEnabled = false
            isBuildingsEnabled = true
            uiSettings.apply {
                isZoomControlsEnabled = true
                isZoomGesturesEnabled = true
            }
            setMyLocationButtonEnabled(this)
            setOnMarkerClickListener(this@ShopsMapFragment)
        }
    }

    private fun setMyLocationButtonEnabled(googleMap: GoogleMap) {
        try {
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = true
        } catch (ex: SecurityException) {
            ex.printStackTrace()
        }
    }


    private fun createViewModel() {
        viewModel = ViewModelProviders
            .of(requireActivity(), ShopListViewModelFactory(RepositoryModule.shopRepository))
            .get(ShopListViewModel::class.java)
    }

    private fun subscribeToViewModel() {
        viewModel.isLoading.subscribe(::showIsLoading).addTo(viewModelDisposable)
        viewModel.isError.subscribe(::showIsError).addTo(viewModelDisposable)
        viewModel.isMainContentVisible.subscribe(::showIsMainContentVisible).addTo(viewModelDisposable)
        viewModel.shops.subscribe(::showAddressesOnMap).addTo(viewModelDisposable)
    }

    private fun unsubscribeFromViewModel() {
        viewModelDisposable.clear()
    }


    private fun showIsLoading(isLoading: Boolean) {
        progressBarLoading.visibility = isLoading.toVisibility()
    }

    private fun showIsError(isError: Boolean) {
        layoutError.visibility = isError.toVisibility()
    }

    private fun showIsMainContentVisible(isVisible: Boolean) {
        mapFragment?.view?.visibility = isVisible.toVisibility()
    }

    private fun showAddressesOnMap(shops: List<Shop>) {
        val bounds = LatLngBounds.Builder()
        shops.forEach { shop ->
            shop.addresses.forEach { address ->
                showAddressOnMap(shop.name, address)
                bounds.include(LatLng(address.latitude, address.longitude))
            }
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 200))
    }

    private fun showAddressOnMap(shopName: String, address: Address) {
        val location = LatLng(address.latitude, address.longitude)
        val markerOptions = MarkerOptions()
            .position(location)
            .title(address.address)
        googleMap.addMarker(markerOptions).tag = Pair(shopName, address)
    }

    private fun showAddressBottomSheetDialog(shopName: String, address: Address) {
        AddressBottomSheetDialogFragment.newInstance(address, shopName).show(childFragmentManager, "")
    }
}