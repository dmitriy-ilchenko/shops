package com.example.shops.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shops.R
import com.example.shops.RepositoryModule
import com.example.shops.activity.ShopDetailsActivity
import com.example.shops.adapter.ShopListAdapter
import com.example.shops.common.toVisibility
import com.example.shops.model.Shop
import com.example.shops.viewmodel.ShopListViewModel
import com.example.shops.viewmodel.ShopListViewModelFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_shop_list.*

class ShopListFragment : Fragment() {
    private lateinit var viewModel: ShopListViewModel
    private val viewModelDisposable = CompositeDisposable()

    private val shopListAdapter = ShopListAdapter(::startShopActivity)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_shop_list, container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initShopList()
        initEventHandlers()
    }

    override fun onStart() {
        super.onStart()
        subscribeToViewModel()
    }

    override fun onStop() {
        super.onStop()
        unsubscribeFromViewModel()
    }


    private fun initShopList() {
        recyclerViewShopList.layoutManager = LinearLayoutManager(context)
        recyclerViewShopList.adapter = shopListAdapter
    }

    private fun initEventHandlers() {
        swipeRefreshLayoutShopList.setOnRefreshListener {
            swipeRefreshLayoutShopList.isRefreshing = false
            viewModel.loadShops()
        }

        buttonLoadShops.setOnClickListener {
            viewModel.loadShops()
        }
    }


    private fun createViewModel() {
        viewModel = ViewModelProviders
            .of(requireActivity(), ShopListViewModelFactory(RepositoryModule.shopRepository))
            .get(ShopListViewModel::class.java)
    }

    private fun subscribeToViewModel() {
        viewModel.isLoading.subscribe(::showIsLoading).addTo(viewModelDisposable)
        viewModel.isError.subscribe(::showIsError).addTo(viewModelDisposable)
        viewModel.isMainContentVisible.subscribe(::showIsMainContentVisible).addTo(viewModelDisposable)
        viewModel.shops.subscribe(::showShops).addTo(viewModelDisposable)
    }

    private fun unsubscribeFromViewModel() {
        viewModelDisposable.clear()
    }


    private fun showIsLoading(isLoading: Boolean) {
        progressBarLoading.visibility = isLoading.toVisibility()
    }

    private fun showIsError(isError: Boolean) {
        layoutError.visibility = isError.toVisibility()
    }

    private fun showIsMainContentVisible(isVisible: Boolean) {
        swipeRefreshLayoutShopList.visibility = isVisible.toVisibility()
    }

    private fun showShops(shops: List<Shop>) {
        shopListAdapter.setData(shops)
    }


    private fun startShopActivity(shopId: Int) {
        ShopDetailsActivity.start(requireContext(), shopId)
    }
}