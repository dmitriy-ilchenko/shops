package com.example.shops.fragment.dialog

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.shops.R
import com.example.shops.model.Address
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_map_bottom_sheet_dialog.*

class AddressBottomSheetDialogFragment : BottomSheetDialogFragment() {

    companion object {
        private const val shopNameArgumentsKey = "shopNameArgumentsKey"
        private const val addressArgumentsKey = "addressArgumentsKey"

        fun newInstance(address: Address, shopName: String = ""): AddressBottomSheetDialogFragment {
            val args = Bundle()
            args.putSerializable(addressArgumentsKey, address)
            args.putString(shopNameArgumentsKey, shopName)

            val fragment = AddressBottomSheetDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map_bottom_sheet_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showShopName()
        showAddress()
    }


    private fun showShopName() {
        val shopName = arguments?.getString(shopNameArgumentsKey) ?: return
        showTextInTextView(shopName, textViewShopNameHint, textViewShopName)
    }

    private fun showAddress() {
        val address = arguments?.getSerializable(addressArgumentsKey) as? Address ?: return

        showTextInTextView(address.address, textViewAddressHint, textViewAddress)
        showTextInTextView(address.schedule, textViewScheduleHint, textViewSchedule)
        showTextInTextView(address.phone, textViewPhoneHint, textViewPhone)

        if (address.phone.isEmpty()) {
            return
        }

        spaceMakePhoneCallButton.visibility = View.VISIBLE
        buttonMakePhoneCall.visibility = View.VISIBLE
        buttonMakePhoneCall.setOnClickListener { makePhoneCall(address.phone) }
    }

    private fun showTextInTextView(text: String, hintTextView: TextView, textView: TextView) {
        if (text.isEmpty()) {
            return
        }

        hintTextView.visibility = View.VISIBLE
        textView.visibility = View.VISIBLE
        textView.text = text
    }

    private fun makePhoneCall(phone: String) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:$phone")
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }

        val packageManager = activity?.packageManager ?: return

        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }
}