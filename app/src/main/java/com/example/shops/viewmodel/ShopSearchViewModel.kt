package com.example.shops.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shops.model.Shop
import com.example.shops.repository.MockShopRepository
import com.example.shops.repository.ShopRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class ShopSearchViewModelFactory(private val shopRepository: ShopRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ShopSearchViewModel(shopRepository) as T
    }
}

class ShopSearchViewModel(private val shopRepository: ShopRepository) : ViewModel() {
    private val searchTextSubject = PublishSubject.create<String>()
    private val disposable = CompositeDisposable()

    val isLoading = BehaviorSubject.create<Boolean>()
    val isError = BehaviorSubject.create<Boolean>()
    val isMainContentVisible = BehaviorSubject.create<Boolean>()
    val shopsSearchResult = BehaviorSubject.create<Pair<String, List<Shop>>>()

    init {
        searchTextSubject
            .distinctUntilChanged()
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribe { searchShops(it) }
            .addTo(disposable)
    }

    fun handleSearchTextChanged(searchText: String) {
        searchTextSubject.onNext(searchText)
    }

    private fun searchShops(searchText: String) {
        isError.onNext(false)
        isMainContentVisible.onNext(false)
        isLoading.onNext(true)

        shopRepository.searchShops(searchText)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { isLoading.onNext(false) }
            .subscribe(
                { shopList ->
                    shopsSearchResult.onNext(Pair(searchText, shopList))
                    isMainContentVisible.onNext(true)
                },
                {
                    isError.onNext(true)
                }
            )
            .addTo(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}