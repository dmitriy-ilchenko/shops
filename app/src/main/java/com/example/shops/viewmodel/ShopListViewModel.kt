package com.example.shops.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shops.model.Shop
import com.example.shops.repository.MockShopRepository
import com.example.shops.repository.ShopRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class ShopListViewModelFactory(private val shopRepository: ShopRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ShopListViewModel(shopRepository) as T
    }
}

class ShopListViewModel(private val shopRepository: ShopRepository) : ViewModel() {
    private val disposable = CompositeDisposable()

    val isLoading = BehaviorSubject.create<Boolean>()
    val isError = BehaviorSubject.create<Boolean>()
    val isMainContentVisible = BehaviorSubject.create<Boolean>()
    val shops = BehaviorSubject.create<List<Shop>>()

    init {
        loadShops()
    }

    fun loadShops() {
        isError.onNext(false)
        isMainContentVisible.onNext(false)
        isLoading.onNext(true)

        shopRepository.getAllShops()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { isLoading.onNext(false) }
            .subscribe(
                { shopList ->
                    shops.onNext(shopList)
                    isMainContentVisible.onNext(true)
                },
                {
                    isError.onNext(true)
                }
            )
            .addTo(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}