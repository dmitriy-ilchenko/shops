package com.example.shops.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shops.model.Shop
import com.example.shops.repository.MockShopRepository
import com.example.shops.repository.ShopRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class ShopDetailsViewModelFactory(
    private val shopRepository: ShopRepository,
    private val shopId: Int
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ShopDetailsViewModel(shopRepository, shopId) as T
    }
}

class ShopDetailsViewModel(private val shopRepository: ShopRepository, private val shopId: Int) : ViewModel() {
    private val disposable = CompositeDisposable()

    val isLoading = BehaviorSubject.create<Boolean>()
    val isError = BehaviorSubject.create<Boolean>()
    val isMainContentVisible = BehaviorSubject.create<Boolean>()
    val shop = BehaviorSubject.create<Shop>()

    init {
        loadShop()
    }

    fun loadShop() {
        isError.onNext(false)
        isMainContentVisible.onNext(false)
        isLoading.onNext(true)

        shopRepository.getShopById(shopId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { isLoading.onNext(false) }
            .subscribe(
                {
                    shop.onNext(it)
                    isMainContentVisible.onNext(true)
                },
                {
                    isError.onNext(true)
                }
            )
            .addTo(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}